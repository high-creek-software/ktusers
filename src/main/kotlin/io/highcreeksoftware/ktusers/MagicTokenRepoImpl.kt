package io.highcreeksoftware.ktusers

import org.jetbrains.exposed.dao.id.UUIDTable
import org.jetbrains.exposed.sql.*
import org.jetbrains.exposed.sql.`java-time`.datetime
import org.jetbrains.exposed.sql.transactions.transaction
import java.time.LocalDateTime
import java.util.*

class MagicTokenRepoImpl(val db: Database): MagicTokenRepo {

    init {
        transaction(db) {
            SchemaUtils.createMissingTablesAndColumns(MagicTokenDsl)
        }
    }

    override fun store(userId: UUID, token: String): UUID {
        val id = transaction(db) {
            MagicTokenDsl.insertAndGetId {
                it[this.userId] = userId
                it[this.token] = token
                it[created] = LocalDateTime.now()
            }
        }
        return id.value
    }

    override fun getById(id: UUID): MagicToken? {
        var magicToken: MagicToken? = null
        transaction(db) {
            val resultRow = MagicTokenDsl.select{MagicTokenDsl.id eq id}.firstOrNull()
            if(resultRow != null) {
                magicToken = MagicToken().apply {
                    this.id = id
                    this.userId = resultRow[MagicTokenDsl.userId]
                    this.token = resultRow[MagicTokenDsl.token]
                    this.created = resultRow[MagicTokenDsl.created]
                }
            }
        }
        return magicToken
    }

    override fun delete(id: UUID) {
        transaction(db) {
            MagicTokenDsl.deleteWhere { MagicTokenDsl.id eq id }
        }
    }

    private object MagicTokenDsl: UUIDTable("magic_token") {
        val userId = uuid("user_id")
        val token = varchar("token", 512)
        val created = datetime("created")
    }
}