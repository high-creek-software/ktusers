package io.highcreeksoftware.ktusers

import io.javalin.core.security.Role
import org.jetbrains.exposed.sql.*
import org.jetbrains.exposed.sql.transactions.transaction
import java.util.*

class UserRepoImpl<R>(val db: Database, val rClass: Class<R>): UserRepo<R> where R : Enum<R>, R: Role {

    init {
        transaction(db) {
            addLogger(StdOutSqlLogger)
            SchemaUtils.createMissingTablesAndColumns(UserDsl)
        }
    }

    override fun store(user: User<R>): User<R> {
        val id = transaction(db) {
            UserDsl.insertAndGetId {
                it[email] = user.email
                it[firstName] = user.firstName
                it[lastName] = user.lastName
                it[password] = user.password
                it[created] = user.created
                it[role] = user.role.toString()
                it[bio] = user.bio
                it[avatar] = user.avatar
            }
        }
        user.id = id.value
        return user
    }

    override fun hasRole(role: R): Boolean {
        return transaction(db) {
            UserDsl.select{UserDsl.role eq role.toString()}.count() > 0
        }
    }

    override fun loginRequest(email: String): UserLoginCheck? {
        return transaction(db) {
            val resultRow = UserDsl.select{UserDsl.email eq email}.firstOrNull()
            if(resultRow == null) {
                null
            } else {
                UserLoginCheck().apply {
                    id = resultRow[UserDsl.id].value
                    this.email = resultRow[UserDsl.email]
                    password = resultRow[UserDsl.password]
                }
            }
        }
    }

    override fun userById(id: UUID): User<R>? {
        return transaction(db) {
            val resultRow = UserDsl.select { UserDsl.id eq id }.firstOrNull() ?: return@transaction null

            User<R>().apply {
                this.id = id
                email = resultRow[UserDsl.email]
                firstName = resultRow[UserDsl.firstName]
                lastName = resultRow[UserDsl.lastName]
                created = resultRow[UserDsl.created]
                role = java.lang.Enum.valueOf(rClass, resultRow[UserDsl.role])
                bio = resultRow[UserDsl.bio]
                avatar = resultRow[UserDsl.avatar]
            }
        }
    }

    override fun userForMagicToken(email: String): User<R>? {
        return transaction(db) {
            val resultRow = UserDsl.slice(UserDsl.id, UserDsl.firstName, UserDsl.lastName).select{UserDsl.email eq email}.firstOrNull() ?: return@transaction null

            User<R>().apply {
                this.id = resultRow[UserDsl.id].value
                this.firstName = resultRow[UserDsl.firstName]
                this.lastName = resultRow[UserDsl.lastName]
                this.email = email
            }
        }
    }
}