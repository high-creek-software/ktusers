package io.highcreeksoftware.ktusers

import java.io.Serializable
import java.time.LocalDateTime
import java.util.*

data class UserAuthToken(var id: UUID? = null, var token: String = "",
                         var fingerPrint: String = "",
                         var userId: UUID? = null,
                         var rememberMe: Boolean = false,
                         var expires: LocalDateTime? = null): Serializable