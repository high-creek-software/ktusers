package io.highcreeksoftware.ktusers

import java.time.LocalDateTime
import java.util.*

class MagicToken {
    var id: UUID? = null
    var userId:  UUID? = null
    var token: String = ""
    var created: LocalDateTime? = null
}