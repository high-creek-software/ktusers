package io.highcreeksoftware.ktusers

import io.javalin.core.security.Role
import java.util.*

interface UserRepo<R> where R: Enum<R>, R : Role {
    fun store(user: User<R>): User<R>
    fun hasRole(role: R): Boolean
    fun loginRequest(email: String): UserLoginCheck?
    fun userById(id: UUID): User<R>?
    fun userForMagicToken(email: String): User<R>?
}