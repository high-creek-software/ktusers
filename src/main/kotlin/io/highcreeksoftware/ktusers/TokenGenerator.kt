package io.highcreeksoftware.ktusers

import java.security.SecureRandom
import java.util.*

class TokenGenerator {
    val secureRandom = SecureRandom()

    fun generateSecureToken(size: Int = 256): String {
        val bytes = ByteArray(size)
        secureRandom.nextBytes(bytes)
        val encoder = Base64.getUrlEncoder().withoutPadding()
        return encoder.encodeToString(bytes)
    }
}