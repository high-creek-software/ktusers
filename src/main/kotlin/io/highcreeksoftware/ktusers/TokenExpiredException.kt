package io.highcreeksoftware.ktusers

class TokenExpiredException(message: String): Exception(message) {
}