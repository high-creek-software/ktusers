package io.highcreeksoftware.ktusers

import io.javalin.core.security.Role

enum class UserRole: Role {
    Observer, Editor, Admin
}