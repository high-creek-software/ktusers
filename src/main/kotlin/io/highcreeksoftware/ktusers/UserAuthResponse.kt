package io.highcreeksoftware.ktusers

import io.javalin.core.security.Role

data class UserAuthResponse<R>(val user: User<R>, val userAuthToken: UserAuthToken) where R: Enum<R>, R: Role