package io.highcreeksoftware.ktusers

import io.highcreeksoftware.render.HbsMessage
import io.highcreeksoftware.smokesignal.email.EmailAddress
import io.highcreeksoftware.smokesignal.email.EmailMessage
import org.mindrot.jbcrypt.BCrypt
import java.time.LocalDateTime
import java.util.*
import io.highcreeksoftware.smokesignal.email.EmailMessenger

class UserManager<R>(val userRepo: UserRepo<R>, val userAuthTokenRepo: UserAuthTokenRepo, val tokenGenerator: TokenGenerator, val emailMessenger: EmailMessenger?, val magicTokenRepo: MagicTokenRepo?, val hbsMessage: HbsMessage?) where R: Enum<R>, R : io.javalin.core.security.Role {

    private var magicTokenMessageTemplate = "magic-token"

    fun registerAdmin(email: String, firstName: String, lastName: String, password: String, admin: R) {
        val user = User<R>().apply {
            this.email = email
            this.firstName = firstName
            this.lastName = lastName
            this.created = LocalDateTime.now()
            this.role = admin
        }
        val crypt = BCrypt.hashpw(password, BCrypt.gensalt())
        user.password = crypt
        userRepo.store(user)
    }

    fun hasRole(role: R): Boolean = userRepo.hasRole(role)

    fun tryLogin(email: String, password: String, fingerPrint: String): UserAuthResponse<R>? {
        val userLoginCheck = userRepo.loginRequest(email) ?: return null
        if(!BCrypt.checkpw(password, userLoginCheck.password)) {
            return null
        }

        val user = userRepo.userById(userLoginCheck.id!!) ?: return null

        val localDateTime = LocalDateTime.now().plusMonths(3)
        val userAuthToken = UserAuthToken(token = tokenGenerator.generateSecureToken(64),
        fingerPrint = fingerPrint, userId = user.id!!, rememberMe = true, expires = localDateTime)

        userAuthTokenRepo.store(userAuthToken)

        return UserAuthResponse(user, userAuthToken)
    }

    fun initiateLogin(email: String) {
        val user = userRepo.userForMagicToken(email) ?: return

        val token = tokenGenerator.generateSecureToken(128)
        val cryptToken = BCrypt.hashpw(token, BCrypt.gensalt())
        val tokenId = magicTokenRepo!!.store(user.id!!, cryptToken)

        val data: MutableMap<String, Any> = mutableMapOf("token" to token, "id" to tokenId)
        val message = hbsMessage!!.render("magic-token", data)

        emailMessenger!!.send(EmailAddress(user.firstName, user.email), EmailAddress("login", "login@example.com"), EmailMessage("Here is your login link", message, null))
    }

    @Throws(TokenInvalidException::class, TokenExpiredException::class)
    fun redeemLogin(token: String, magicTokenId: UUID, fingerprint: String): UserAuthResponse<R>? {
        val magicToken = magicTokenRepo!!.getById(magicTokenId) ?: return null
        val tokenValid = BCrypt.checkpw(token, magicToken.token)
        if(!tokenValid) {
            throw TokenInvalidException("Token is invalid")
        }

        val rightNow = LocalDateTime.now()
        if(rightNow.plusMinutes(-10).isAfter(magicToken.created)) {
            throw TokenExpiredException("Token is no longer valid")
        }

        val user = userRepo.userById(magicToken.userId!!) ?: return null

        val expiration = rightNow.plusMonths(3)
        val userAuthToken = UserAuthToken(token = tokenGenerator.generateSecureToken(64), fingerPrint = fingerprint, userId = magicToken.userId!!, rememberMe = true, expires = expiration)

        userAuthTokenRepo.store(userAuthToken)

        magicTokenRepo.delete(magicTokenId)

        return UserAuthResponse(user, userAuthToken)
    }

    fun userAuthToken(token: String): UserAuthToken? = userAuthTokenRepo.getByToken(token)

    fun user(userId: UUID): User<R>? = userRepo.userById(userId)
}