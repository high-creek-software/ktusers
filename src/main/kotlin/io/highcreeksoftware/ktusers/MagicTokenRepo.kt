package io.highcreeksoftware.ktusers

import java.util.*

interface MagicTokenRepo {
    fun store(userId: UUID, token: String): UUID
    fun getById(id: UUID): MagicToken?
    fun delete(id: UUID)
}