package io.highcreeksoftware.ktusers

interface UserAuthTokenRepo {
    fun store(userAuthToken: UserAuthToken)
    fun getByToken(token: String): UserAuthToken?
}