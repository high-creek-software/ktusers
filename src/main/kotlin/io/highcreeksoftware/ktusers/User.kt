package io.highcreeksoftware.ktusers

import io.javalin.core.security.Role
import java.io.Serializable
import java.time.LocalDateTime
import java.util.*

data class User<R> (
    var id: UUID? = null,
    var email: String = "",
    var firstName: String = "",
    var lastName: String = "",
    var password: String = "",
    var created: LocalDateTime? = null,
    var role: R? = null,
    var bio: String = "",
    var avatar: String = ""
): Serializable where R : Enum<R>, R: Role
