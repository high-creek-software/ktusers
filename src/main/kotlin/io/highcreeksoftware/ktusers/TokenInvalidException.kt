package io.highcreeksoftware.ktusers

class TokenInvalidException(message: String): Exception(message) {
}