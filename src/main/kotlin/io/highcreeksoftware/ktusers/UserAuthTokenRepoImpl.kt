package io.highcreeksoftware.ktusers

import org.jetbrains.exposed.dao.id.UUIDTable
import org.jetbrains.exposed.sql.*
import org.jetbrains.exposed.sql.`java-time`.datetime
import org.jetbrains.exposed.sql.transactions.transaction

class UserAuthTokenRepoImpl(val db: Database): UserAuthTokenRepo {

    init {
        transaction(db) {
            addLogger(StdOutSqlLogger)
            SchemaUtils.createMissingTablesAndColumns(UserAuthTokenDsl)
        }
    }
    override fun store(userAuthToken: UserAuthToken) {
        val id = transaction(db) {
            UserAuthTokenDsl.insertAndGetId {
                it[token] = userAuthToken.token
                it[fingerPrint] = userAuthToken.fingerPrint
                it[userId] = userAuthToken.userId!!
                it[rememberMe] = userAuthToken.rememberMe
                it[expires] = userAuthToken.expires!!
            }
        }
        userAuthToken.id = id.value
    }

    override fun getByToken(token: String): UserAuthToken? {
        return transaction(db) {
            val resultRow = UserAuthTokenDsl.select { UserAuthTokenDsl.token eq token}.firstOrNull()

            if(resultRow == null) {
                null
            } else {
                UserAuthToken().apply {
                    id = resultRow[UserAuthTokenDsl.id].value
                    this.token = token
                    fingerPrint = resultRow[UserAuthTokenDsl.fingerPrint]
                    userId = resultRow[UserAuthTokenDsl.userId]
                    rememberMe = resultRow[UserAuthTokenDsl.rememberMe]
                    expires = resultRow[UserAuthTokenDsl.expires]
                }
            }
        }
    }

    private object UserAuthTokenDsl: UUIDTable("user_auth_token") {
        val token = varchar("token", 512).index()
        val fingerPrint = varchar("finger_print", 2048)
        val userId = uuid("user_id").index()
        val rememberMe = bool("remember_me")
        val expires = datetime("expires")
    }
}