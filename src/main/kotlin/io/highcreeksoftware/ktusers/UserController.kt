package io.highcreeksoftware.ktusers

import io.highcreeksoftware.render.flashes.FlashManager
import io.javalin.core.security.Role
import io.javalin.http.Context
import io.highcreeksoftware.fortunecookie.FortuneCookie
import io.javalin.http.NotFoundResponse
import io.javalin.http.UnauthorizedResponse
import java.util.*
import javax.servlet.http.Cookie

class UserController<R>(val userManager: UserManager<R>, val admin: R, val flashManager: FlashManager, val fortuneCookie: FortuneCookie? = null) where R: Enum<R>, R: Role {

    fun getRegisterAdmin(ctx: Context) {
        if(userManager.hasRole(admin)) {
            flashManager.setErrorFlash("Admin user exists", ctx)
            ctx.redirect("/user/login")
            return
        }
        ctx.render("frontend/register-admin.hbs")
    }

    fun doRegisterAdmin(ctx: Context) {
        val email = ctx.formParam("email")
        val firstName = ctx.formParam("firstName")
        val lastName = ctx.formParam("lastName")
        val password = ctx.formParam("password")
        if(email == null) {
            flashManager.setErrorFlash("email required", ctx)
            ctx.redirect("/user/register-admin")
            return
        }

        if(firstName == null) {
            flashManager.setErrorFlash("first name required", ctx)
            ctx.redirect("/user/register-admin")
            return
        }

        if(lastName == null) {
            flashManager.setErrorFlash("last name required", ctx)
            ctx.redirect("/user/register-admin")
            return
        }

        if(password == null) {
            flashManager.setErrorFlash("password required", ctx)
            ctx.redirect("/user/register-admin")
            return
        }

        userManager.registerAdmin(email, firstName, lastName, password, admin)
        flashManager.setSuccessFlash("admin user registered", ctx)
        ctx.redirect("/user/login")
    }

    fun getLogin(ctx: Context) {
        ctx.render("frontend/login.hbs")
    }

    fun doLogin(ctx: Context) {
        val email = ctx.formParam("email")
        val password = ctx.formParam("password")

        if(email == null) {
            flashManager.setErrorFlash("email required", ctx)
            ctx.redirect("/user/login")
            return
        }

        if(password == null) {
            flashManager.setErrorFlash("password required", ctx)
            ctx.redirect("/user/login")
            return
        }

        val authResponse = userManager.tryLogin(email, password, ctx.userAgent() ?: "unknown")
        if(authResponse == null) {
            flashManager.setErrorFlash("error with login credentials", ctx)
            ctx.redirect("/user/login")
            return
        }

//        val msg = secureCookie.formatMessage(authResponse.userAuthToken.token)
//        val uat = Cookie(Constants.COOKIE_USER_AUTH_TOKEN_KEY, msg).apply {
//            isHttpOnly = true
//            maxAge = 60 * 60 * 24 * 30 * 3
//        }
//        ctx.cookie(uat)
        ctx.sessionAttribute("user_token", authResponse.userAuthToken.token)
        ctx.redirect("/app/dashboard")
    }

    fun getMagicToken(ctx: Context) {
        ctx.render("frontend/magic-token.hbs")
    }

    fun doMagicToken(ctx: Context) {
        val email = ctx.formParam("email")
        if(email == null) {
            flashManager.setErrorFlash("error: email required", ctx)
            ctx.redirect("/user/magic-token")
            return
        }

        userManager.initiateLogin(email)

        ctx.render("frontend/magic-token-check-email.hbs")
    }

    fun claimMagicToken(ctx: Context) {
        val token = ctx.queryParam("token") ?: throw NotFoundResponse("token is required")
        val tokenId = ctx.queryParam("mid") ?: throw NotFoundResponse("token id is required")

        val authResponse = userManager.redeemLogin(token, UUID.fromString(tokenId), ctx.userAgent() ?: "unknown") ?: throw UnauthorizedResponse()

        ctx.sessionAttribute("user_token", authResponse.userAuthToken.token)
        ctx.redirect("/app/dashboard")
    }

    fun getLogout(ctx: Context) {
        ctx.req.session.invalidate()
        ctx.redirect("/")
    }
}