package io.highcreeksoftware.ktusers

import org.jetbrains.exposed.dao.id.UUIDTable
import org.jetbrains.exposed.sql.`java-time`.datetime

object UserDsl: UUIDTable(name = "users", columnName = "users_id") {
    val email = varchar("email", 256).index()
    val firstName = varchar("first_name", 128)
    val lastName = varchar("last_name", 128)
    var password = varchar("password", 2048)
    var created = datetime("created").nullable()
    var role = varchar("user_role", 128).index()
    var bio = text("bio")
    var avatar = varchar("avatar", 2048)
}