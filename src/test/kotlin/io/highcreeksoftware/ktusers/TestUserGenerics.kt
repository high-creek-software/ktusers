package io.highcreeksoftware.ktusers

import com.google.gson.Gson
import io.highcreeksoftware.render.HbsMessage
import io.highcreeksoftware.smokesignal.email.EmailAddress
import io.highcreeksoftware.smokesignal.email.EmailMessage
import org.jetbrains.exposed.sql.Database
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import io.highcreeksoftware.smokesignal.email.EmailMessenger
import io.highcreeksoftware.smokesignal.email.LogMessenger

class TestUserGenerics {

    lateinit var db: Database
    lateinit var userRepo: UserRepo<TestUserRoles>
    lateinit var userTokenRepo: UserAuthTokenRepo
    lateinit var tokenGenerator: TokenGenerator
    lateinit var emailMessenger: EmailMessenger
    lateinit var magicTokenRepo: MagicTokenRepo
    lateinit var hbsMessage: HbsMessage

    @Before
    fun initDb() {
        db = Database.connect("jdbc:h2:mem:regular;DB_CLOSE_DELAY=-1;", driver = "org.h2.Driver")
        userTokenRepo = UserAuthTokenRepoImpl(db)
        userRepo = UserRepoImpl(db, TestUserRoles::class.java)
        tokenGenerator = TokenGenerator()
        emailMessenger = LogMessenger()
        magicTokenRepo = MagicTokenRepoImpl(db)
        hbsMessage = HbsMessage(false)
    }

    @Test
    fun testUserRepo() {
        val userManager = UserManager<TestUserRoles>(userRepo, userTokenRepo, tokenGenerator, emailMessenger, magicTokenRepo, hbsMessage)
        userManager.registerAdmin("dummy@example.com", "John", "Doe", "abc123", TestUserRoles.Owner)

        Assert.assertTrue(userManager.hasRole(TestUserRoles.Owner))

        val authResponse = userManager.tryLogin("dummy@example.com", "abc123", "TEST")

        Assert.assertNotNull(authResponse)
        Assert.assertNotNull(authResponse!!.user)
        Assert.assertEquals(TestUserRoles.Owner, authResponse.user.role)
    }

    @Test
    fun testMagicLink() {
        val testEmailMessenger = TestEmailMessenger()
        val userManager = UserManager<TestUserRoles>(userRepo, userTokenRepo, tokenGenerator, testEmailMessenger, magicTokenRepo, hbsMessage)
        userManager.registerAdmin("magic@example.com", "John", "Doe", "", TestUserRoles.Owner)
        Assert.assertTrue(userManager.hasRole(TestUserRoles.Owner))

        userManager.initiateLogin("magic@example.com")

        Assert.assertNotNull(testEmailMessenger.mostRecentMessage)
        Assert.assertNotNull(testEmailMessenger.mostRecentMessage?.plain)
        val gson = Gson()

        val magicTokenParsed = gson.fromJson(testEmailMessenger.mostRecentMessage?.plain, MagicToken::class.java)

        val userAuthResponse = userManager.redeemLogin(magicTokenParsed.token, magicTokenParsed.id!!, "tests")
        Assert.assertNotNull(userAuthResponse)

        Assert.assertEquals("magic@example.com", userAuthResponse!!.user.email)
    }

    class TestEmailMessenger: EmailMessenger {
        var mostRecentMessage: EmailMessage? = null
        var mostRecentTo: EmailAddress? = null
        var mostRecentFrom: EmailAddress? = null

        override fun send(to: EmailAddress, from: EmailAddress, message: EmailMessage) {

            mostRecentMessage = message
            mostRecentTo = to
            mostRecentFrom = from

            println("To: ${to.name} <${to.address}>")
            println("From: ${from.name} <${from.address}>")
            println(message.plain)
            println("----------")
            println(message.html)
        }
    }
}