package io.highcreeksoftware.ktusers

import io.javalin.core.security.Role

enum class TestUserRoles: Role {
    Observer, Editor, Owner
}