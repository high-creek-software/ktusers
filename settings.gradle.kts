pluginManagement {
    repositories {
        gradlePluginPortal()
    }
    resolutionStrategy {
        eachPlugin {
            if(requested.id.id == "org.jetbrains.kotlin.jvm") {
                useModule("org.jetbrains.kotlin:kotlin-gradle-plugin:1.4.10")
            }
        }
    }
}

rootProject.name = "ktusers"

sourceControl {
    gitRepository(java.net.URI("https://gitlab.com/high-creek-software/render.git")) {
        producesModule("io.highcreeksoftware:render")
    }
    gitRepository(java.net.URI("https://gitlab.com/high-creek-software/fortune-cookie.git")) {
        producesModule("io.highcreeksoftware:fortunecookie")
    }
    gitRepository(java.net.URI("https://gitlab.com/high-creek-software/smoke-signal.git")) {
        producesModule("io.highcreeksoftware:smokesignal")
    }
}

//include(":smokesignal")
//project(":smokesignal").projectDir = File("../smoke-signal")